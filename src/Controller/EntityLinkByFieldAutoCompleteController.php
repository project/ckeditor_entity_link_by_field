<?php

namespace Drupal\ckeditor_entity_link_by_field\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\Element\EntityAutocomplete;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class EntityLinkByFieldAutoCompleteController extends ControllerBase {

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorage
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {

    $config = $this->config('ckeditor_entity_link_by_field.settings');

    $results = [];
    $input = $request->query->get('q');

    // Get the typed string from the URL, if it exists.
    if (!$input) {
      return new JsonResponse($results);
    }

    $input = Xss::filter($input);

    $fieldname = FALSE;

    $sources = !empty($config->get('sources')) ? $config->get('sources') : [];
    if (!empty($sources)) {
      foreach ($sources as $values) {
        if ($values['entity_type'] === 'node') {
          $fieldname = $values['field'];
          break;
        }
      }
    }

    if (!$fieldname) {
      return new JsonResponse([]);
    }

    $query = $this->nodeStorage->getQuery()
      ->condition('type', 'article')
      ->condition($fieldname, $input, 'CONTAINS')
      ->groupBy('nid')
      ->sort('field_machine_name')
      ->range(0, 10);

    $ids = $query->execute();
    $nodes = $ids ? $this->nodeStorage->loadMultiple($ids) : [];

    foreach ($nodes as $node) {
      switch ($node->isPublished()) {
        case TRUE:
          $availability = '✅';
          break;

        case FALSE:
        default:
          $availability = '🚫';
          break;
      }

      $label = [
        $node->get('field_machine_name')->getString(),
        '<small>(' . $node->id() . ')</small>',
        $availability,
      ];

      $results[] = [
        'value' => EntityAutocomplete::getEntityLabels([$node]),
        'label' => implode(' ', $label),
      ];
    }

    return new JsonResponse($results);
  }
}

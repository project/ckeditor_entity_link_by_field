<?php

namespace Drupal\ckeditor_entity_link_by_field\Form;

use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CKEditorEntityLinkByFieldConfigForm.
 *
 * @package Drupal\ckeditor_entity_link_by_field\Form
 */
class CKEditorEntityLinkByFieldConfigForm extends ConfigFormBase {

  /**
   * The entity type id.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The entity type id.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info, EntityFieldManager $entity_field_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $bundle_info;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ckeditor_entity_link_by_field_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ckeditor_entity_link_by_field.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ckeditor_entity_link_by_field.settings');

    // Fieldset for configuration.
    $form['sources'] = [
      '#type' => 'details',
      '#title' => $this->t('Sources'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    // Prepare the entity selector.
    $entity_type_options = [];

    // Get all applicable entity types.
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if (is_subclass_of($entity_type->getClass(), FieldableEntityInterface::class) && $entity_type->hasLinkTemplate('canonical')) {
        $entity_type_options[$entity_type_id] = $entity_type->getLabel();
      }
    }

    $form['sources']['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#options' => $entity_type_options,
      '#empty_option' => $this->t('- Select -'),
      '#limit_validation_errors' => [['sources', 'entity_type']],
      '#submit' => ['::submitSelectEntityType'],
      '#executes_submit_callback' => TRUE,
      '#ajax' => [
        'callback' => '::ajaxReplaceBundles',
        'wrapper' => 'sources-bundle',
        'method' => 'replace',
      ],
    ];

    $form['sources']['bundle_container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="sources-bundle">',
      '#suffix' => '</div>',
    ];

    $form['sources']['field_container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="sources-field">',
      '#suffix' => '</div>',
    ];

    if ($this->entityTypeId) {

      $bundles = $this->entityTypeBundleInfo->getBundleInfo($this->entityTypeId);
      $bundle_options = [];

      foreach ($bundles as $id => $info) {
        $bundle_options[$id] = $info['label'];
      }

      $form['sources']['bundle_container']['bundle'] = [
        '#type' => 'select',
        '#title' => $this->t('Bundle'),
        '#options' => $bundle_options,
        '#empty_option' => $this->t('- Select -'),
        '#submit' => ['::submitSelectBundle'],
        '#executes_submit_callback' => TRUE,
        '#ajax' => [
          'callback' => '::ajaxReplaceFields',
          'wrapper' => 'sources-field',
          'method' => 'replace',
        ],
      ];

    }

    if ($this->bundle) {

      $fields = $this->entityFieldManager->getFieldDefinitions($this->entityTypeId, $this->bundle);
      $field_options = [];

      foreach ($fields as $field_name => $field_definition) {
        if ($field_definition->getFieldStorageDefinition()->isBaseField() === FALSE && $field_definition->getType() === 'string') {
          $field_options[$field_name] = $field_definition->getLabel();
        }
      }

      $form['sources']['field_container']['field'] = [
        '#type' => 'select',
        '#title' => $this->t('Field'),
        '#options' => $field_options,
        '#empty_option' => $this->t('- Select -'),
      ];

    }

    $form['sources']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#validate' => ['::validateAdd'],
      '#submit' => ['::submitAdd'],
      '#button_type' => 'primary',
      '#suffix' => '<p>&nbsp;</p>',
    ];

    // Show and change all custom configurations.
    $form['sources']['active'] = [
      '#type' => 'table',
      '#header' => [
        'entity' => $this->t('Entity type'),
        'bundle' => $this->t('Bundle'),
        'field' => $this->t('Field'),
        'remove' => $this->t('Remove'),
      ],
      '#empty' => $this->t('No configuration added yet.'),
    ];

    if ($sources = $config->get('sources')) {
      foreach ($sources as $key => $settings) {

        $row = [
          'entity_type' => ['#markup' => $settings['entity_type']],
          'bundle' => ['#markup' => $settings['bundle']],
          'field' => ['#markup' => $settings['field']],
        ];

        $row['remove'] = [
          '#type' => 'checkbox',
          '#default_value' => $key,
        ];

        $form['sources']['active'][$key] = $row;
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitAdd(array &$form, FormStateInterface $form_state) {

    $settings = [
      'entity_type' => $form_state->getValue(['sources', 'entity_type']),
      'bundle' => $form_state->getValue([
        'sources',
        'bundle_container',
        'bundle',
      ]),
      'field' => $form_state->getValue([
        'sources',
        'field_container',
        'field',
      ]),

    ];

    $config = $this->config('ckeditor_entity_link_by_field.settings');
    $sources = !empty($config->get('sources')) ? $config->get('sources') : [];

    $key = implode('-', $settings);

    $sources[$key] = $settings;

    $config->set('sources', $sources);
    $config->save();

    $this->messenger()->addMessage($this->t('The configuration has been added.'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ckeditor_entity_link_by_field.settings');
    $sources = !empty($config->get('sources')) ? $config->get('sources') : [];

    if ($form_state->hasValue(['sources', 'active']) && is_array($form_state->getValue(['sources', 'active']))) {
      foreach ($form_state->getValue(['sources', 'active']) as $key => $values) {
        if (!empty($values['remove'])) {
          // Remove the config.
          unset($sources[$key]);
        }
      }
    }

    $config->set('sources', $sources);

    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Handles submit call when entity type is selected.
   */
  public function submitSelectEntityType(array $form, FormStateInterface $form_state) {
    $this->entityTypeId = $form_state->getValue(['sources', 'entity_type']);
    $form_state->setRebuild();
  }

  /**
   * Handles submit call when bundle is selected.
   */
  public function submitSelectBundle(array $form, FormStateInterface $form_state) {
    $this->entityTypeId = $form_state->getValue(['sources', 'entity_type']);
    $this->bundle = $form_state->getValue(['sources', 'bundle_container', 'bundle']);
    $form_state->setRebuild();
  }

  /**
   * Handles switching the type selector.
   */
  public function ajaxReplaceBundles($form, FormStateInterface $form_state) {
    return $form['sources']['bundle_container'];
  }

  /**
   * Handles switching the bundle selector.
   */
  public function ajaxReplaceFields($form, FormStateInterface $form_state) {
    return $form['sources']['field_container'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateAdd(array &$form, FormStateInterface $form_state) {
    $entity_type = $form_state->getValue(['sources', 'entity_type']);
    if ($entity_type === '') {
      $form_state->setErrorByName('sources][entity_type', $this->t('You must select an entity type.'));
    }
    else {
      $fields = ['sources', 'bundle_container', 'bundle'];
      if ($form_state->getValue($fields) == '') {
        $form_state->setErrorByName('sources][bundle_container][bundle', $this->t('You must select a bundle.'));
      }
      else {
        if ($form_state->getValue(['sources', 'field_container', 'field']) == '') {
          $form_state->setErrorByName('sources][field_container][field', $this->t('You must select a field.'));
        }
      }
    }

    $exists = FALSE;

    $config = $this->config('ckeditor_entity_link_by_field.settings');
    $sources = !empty($config->get('sources')) ? $config->get('sources') : [];

    if (!empty($sources)) {
      foreach ($sources as $values) {
        if ($values['entity_type'] === $entity_type) {
          $exists = TRUE;
          break;
        }
      }
    }

    if ($exists) {
      $form_state->setErrorByName('sources][entity_type', $this->t('A source for this entity type already exists. You may only have a single source per entity type.'));
    }
  }

}

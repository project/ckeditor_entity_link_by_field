<?php

namespace Drupal\ckeditor_entity_link_by_field\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "entitylinkbyfield" plugin.
 *
 * @CKEditorPlugin(
 *   id = "entitylinkbyfield",
 *   label = @Translation("Entity link"),
 * )
 */
class EntityLinkByField extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'ckeditor_entity_link_by_field') . '/js/plugins/entitylinkbyfield/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/drupal.ajax',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [
      'EntityLinkByField_dialogTitleAdd' => $this->t('Add Link'),
      'EntityLinkByField_dialogTitleEdit' => $this->t('Edit Link'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $path = drupal_get_path('module', 'ckeditor_entity_link_by_field') . '/js/plugins/entitylinkbyfield';
    return [
      'EntityLinkByField' => [
        'label' => $this->t('Link by Field'),
        'image' => $path . '/link.png',
      ],
    ];
  }

}
